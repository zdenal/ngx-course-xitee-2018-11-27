import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { first, mergeMap, tap } from 'rxjs/operators';

import { assign } from '../utils/common.utils';
import { CarsApiService } from './cars-api.service';
import { Car, CarsState } from './cars.model';

@Injectable()
export class CarsService {

  state$: Observable<CarsState>;

  private stateSource = new BehaviorSubject<CarsState>({
    cars: [],
    selectedCar: null,
    loadingCars: false,
    firstLoadingCars: true,
    carsError: false,
    carCreateFormVisible: false
  });

  constructor(
    private api: CarsApiService
  ) {
    this.state$ = this.stateSource.asObservable();
  }

  loadCars(): void {
    this.reduceState('Before loading cars', (state) => {
      state.loadingCars = true;
      state.carsError = false;
      return state;
    });
    this.api.getCars$().subscribe((cars) => {
      this.reduceState('Cars loaded', (state) => {
        state.cars = cars;
        state.loadingCars = false;
        state.firstLoadingCars = false;
        return state;
      });
    }, (err) => {
      this.reduceState('Cars loading error', (state) => {
        state.loadingCars = false;
        state.firstLoadingCars = false;
        state.carsError = true;
        return state;
      });
      console.error(err);
    });
  }

  selectCar(car: Car): void {
    this.reduceState('Select car', (state) => {
      state.selectedCar = car;
      return state;
    });
  }

  showCarCreateForm(): void {
    this.reduceState('Show car create form', (state) => {
      state.selectedCar = null;
      state.carCreateFormVisible = true;
      return state;
    });
  }

  hideCarCreateForm(): void {
    this.reduceState('Show car create form', (state) => {
      state.carCreateFormVisible = false;
      return state;
    });
  }

  showCreatedCar(car: Car): void {
    this.reduceState('Show created car', (state) => {
      state.selectedCar = car;
      state.carCreateFormVisible = false;
      return state;
    });
  }

  getCar$(id: string): Observable<Car> {
    return this.state$.pipe(first(), mergeMap((state) => {
      const car = (state.selectedCar && state.selectedCar.id === id) ? state.selectedCar : state.cars.find((c) => c.id === id);
      if (car) {
        return of(car);
      } else {
        return this.api.getCar$(id);
      }
    }));
  }

  updateCar$(car: Car): Observable<Car> {
    return this.api.updateCar$(car).pipe(tap(updatedCar => {
      this.reduceState('Car updated', (state) => {
        const idx = state.cars.findIndex((c) => c.id === updatedCar.id);
        if (idx > -1) {
          state.cars = [
            ...state.cars.slice(0, idx),
            updatedCar,
            ...state.cars.slice(idx + 1)
          ];
        }
        if (state.selectedCar && state.selectedCar.id === updatedCar.id) {
          state.selectedCar = updatedCar;
        }
        return state;
      });
      this.loadCars();
    }));
  }

  createCar$(car: Car): Observable<Car> {
    return this.api.createCar$(car).pipe(tap(() => {
      this.loadCars();
    }));
  }

  deleteCar$(id: string): Observable<any> {
    return this.api.deleteCar$(id).pipe(tap(() => {
      this.reduceState('Car deleted', (state) => {
        const idx = state.cars.findIndex((c) => c.id === id);
        if (idx > -1) {
          state.cars = [
            ...state.cars.slice(0, idx),
            ...state.cars.slice(idx + 1)
          ];
        }
        return state;
      });
      this.loadCars();
    }));
  }

  private reduceState(action: string, reducer: (state: CarsState) => CarsState): void {
    const actualState = assign(this.stateSource.getValue());
    const nextState = reducer(actualState);
    this.stateSource.next(nextState);
    console.log(`Cars state reduced with action: ${action}`, nextState);
  }

}
