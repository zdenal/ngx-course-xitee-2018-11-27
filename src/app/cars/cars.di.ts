import { InjectionToken } from '@angular/core';

import { CarsConfig } from './cars.model';

export const carsConfigToken = new InjectionToken<CarsConfig>('cars.config');
