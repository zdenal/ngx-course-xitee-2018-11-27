import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Car, Cars, CarsState } from './../cars.model';
import { CarsService } from './../cars.service';

@Component({
  selector: 'app-cars-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit {

  i18NPrefix = 'cars.list.';
  cars: Cars;
  firstLoadingCars = false;
  carsError = false;
  selectedCar: Car;

  carsState$: Observable<CarsState>;

  constructor(
    private carsService: CarsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.carsState$ = this.carsService.state$;
  }

  ngOnInit() {
    this.carsService.loadCars();
  }

  onCarClick(car: Car): void {
    this.router.navigate(['detail', car.id], { relativeTo: this.route });
  }

  onCreateCar(): void {
    this.router.navigate(['create'], { relativeTo: this.route });
  }

}
