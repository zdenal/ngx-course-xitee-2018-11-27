import { Environment } from './environment.model';

export const environment: Environment = {
  production: true,
  firebaseUriPrefix: 'https://ngx-playground.firebaseio.com/common'
};
